/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Wavefront .obj file parser.
 */

#import <Foundation/Foundation.h>

#import <obj.hpp>
#import <tr1/functional>
#import <vector>

#import "ParserProtocol.h"

@interface WavefrontParser : NSObject <ParserProtocol> {
	obj::obj_parser engine;

	std::vector<float *> coords;
	std::vector<float *> normals;
	std::vector<float *> texCoords;

	NSMutableArray *triangles;
}

/**
 * Add vertex coordinate <x,y,z> to cache.
 *
 * @param x The x value
 * @param y The y value
 * @param z The z value
 */
- (void)addVertexToCacheWithX:(float)x andY:(float)y andZ:(float)z;

/**
 * Add normal <x,y,z> to cache.
 *
 * @param x The x value
 * @param y The y value
 * @param z The z value
 */
- (void)addNormalToCacheWithX:(float)x andY:(float)y andZ:(float)z;

/**
 * Add texture coordinate <u,v> to cache.
 *
 * @param u The u value
 * @param v The v value
 */
- (void)addTextureCoordinateToCacheWithU:(float)u andV:(float)v;

/**
 * Add triangular face with 3-vertices <x,y,z> and 3-normals <x,y,z> indexes.
 *
 * @param vertices The vertices index list
 * @param normals_ The normals index list
 */
- (void)addTriangularFaceWithVerticesIndexes:(NSArray *)vertices andNormalsIndexes:(NSArray *)normals_;

/**
 * Transfer all data to the given Object object.
 *
 * @param batch The Object oject
 * @param primitive The OpenGL ES primitive
 */
- (void)commitIntoObject:(Object *)obj withPrimitive:(GLenum)primitive;

/**
 * Clear all temporary cached values.
 */
- (void)uncache;

@end
