/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * OpenGL ES viewer class.
 */

#import "GLView.h"

// Code adapted from "iPhone 3D Programming" (19th Sep. 2011)

@implementation GLView

+ (Class)layerClass {
	return [CAEAGLLayer class];
}

- (void)dealloc {
	if ([EAGLContext currentContext] == context) {
		[EAGLContext setCurrentContext:nil];
		[context release];

		delete engine;

		[super dealloc];
	}
}

- (id)initWithFrame:(CGRect)frame andFillInfo:(RuntimeInfo &)info_ {
	self = [super initWithFrame:frame];
	if (self) {
		EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;

		context = [[EAGLContext alloc] initWithAPI:api];
		if (!context || ![EAGLContext setCurrentContext:context]) {
			NSLog(@"OpenGL ES 2.0 is not supported, quitting...");

			[self release];
			return nil;
		}

		NSLog(@"Using OpenGL ES 2.0");

		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
		eaglLayer.opaque = NO;

		engine = new Render(&info_);
		[context renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		if (!engine->Initialize()) {
			[NSException raise:@"Render initialization error"
						format:@"It was impossible to start the render engine"];
		}

		CADisplayLink *displayLink;
		displayLink = [CADisplayLink displayLinkWithTarget:self
												  selector:@selector(drawView:)];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop]
						  forMode:NSDefaultRunLoopMode];

		info = &info_;
	}
	return self;
}

- (void)drawView:(CADisplayLink *)displayLink {
	engine->Draw();

	[context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)refreshWithMarkers:(std::vector<aruco::Marker> &)markers {
	engine->Clear();
	for (int i=0; i < markers.size(); i++) {
		engine->Push(markers[i]);
	}
}

@end
