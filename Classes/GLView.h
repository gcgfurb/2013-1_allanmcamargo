/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * OpenGL ES viewer class.
 */

#import <OpenGLES/EAGL.h>
#import <OpenGLES/EAGLDrawable.h>
#import <OpenGLES/ES2/gl.h>
#import <QuartzCore/QuartzCore.h>

#import "ObjectManager.h"
#import "Render.h"
#import "Types.h"
#import "Utils.h"

@interface GLView : UIView {
	RuntimeInfo *info;

	EAGLContext *context;

	Render *engine;
}

/**
 * Initialize with boundaries and fill the workspace info.
 *
 * @param frame The frame bounds
 * @param info_ The info for using and filling it
 * @returns The GLView object
 */
- (id)initWithFrame:(CGRect)frame andFillInfo:(RuntimeInfo &)info_;

/**
 * Call render drawing routines.
 *
 * @param displayLink The timer object for render refresh rate
 */
- (void)drawView:(CADisplayLink *)displayLink;

/**
 * Push visible markers and ask for refreshing.
 *
 * @param markers The visible markers
 */
- (void)refreshWithMarkers:(std::vector<aruco::Marker> &)markers;

@end
