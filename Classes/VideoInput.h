/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Video input class.
 */

#import <AVFoundation/AVFoundation.h>

#import "Types.h"
#import "Utils.h"

#if TARGET_OS_EMBEDDED
NSString * const CaptureBufferProcessingDidFinishNotification =
	@"CaptureBufferProcessingDidFinishNotification";

@interface VideoInput : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate> {
	RuntimeInfo *info;

	AVCaptureSession *session;
}

@property (nonatomic, retain) UIView *previewer;
@property (nonatomic, retain) AVCaptureVideoDataOutput *videoOutput;
@property (readonly) cv::Mat &buffer;

/**
 * Initialize with boundaries and fill the workspace info.
 *
 * @param frame The frame bounds
 * @param info_ The info for using and filling it
 * @returns The VideoInput object
 */
- (id)initWithFrame:(CGRect)frame andFillInfo:(RuntimeInfo &)info_;

/**
 * Start video capturing.
 */
- (void)startCapturing;

/**
 * Stop video capturing.
 */
- (void)stopCapturing;

@end
#endif
