/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Render engine class.
 */

#import <aruco.hpp>
#import <GLTools.h>
#import <GLMatrixStack.h>

#import "CppUtils.h"
#import "Object.h"
#import "ObjectManager.h"
#import "Types.h"

class Render {
private:
	RuntimeInfo *info;

	std::vector<aruco::Marker> queue;

	ObjectManager *objManager;

	GLuint program;
	GLuint framebuffer;
	GLuint renderbuffer;
	GLuint depthbuffer;

	Object axis;
	Object box;

public:
	/**
	 * Construct with informations.
	 *
	 * @param info The render info structure
	 */
	Render(RuntimeInfo *info);

	/**
	 * Initialize render buffers and the shader program.
	 *
	 * @returns True for OK, or false if doesn't
	 */
	bool Initialize();

	/**
	 * Change viewport dimensions.
	 *
	 * @param width The width value
	 * @param height The height value
	 */
	void ChangeSize(int width, int height);

	/**
	 * Draw onto the screen.
	 */
	void Draw();

	/**
	 * Create 3D axis for coordinates orientation.
	 */
	void CreateAxis();

	/**
	 * Create 3D box for coordinates orientation.
	 */
	void CreateBox();

	/**
	 ** Push marker into the queue.
	 *
	 * @param marker The Marker object
	 */
	void Push(aruco::Marker &marker);

	/**
	 * Clear all visible markers from queue.
	 */
	void Clear();
};
