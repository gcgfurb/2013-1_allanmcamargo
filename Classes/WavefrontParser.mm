/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Wavefront .obj file parser.
 */

#import "WavefrontParser.h"

static id ref = nil; // This is ugly, but necessary

void geometric_vertex_callback(obj::float_type x, obj::float_type y, obj::float_type z) {
	[ref addVertexToCacheWithX:x andY:y andZ:z];
}

void texture_vertex_callback(obj::float_type u, obj::float_type v) {
	[ref addTextureCoordinateToCacheWithU:u andV:v];
}

void vertex_normal_callback(obj::float_type x, obj::float_type y, obj::float_type z) {
	[ref addNormalToCacheWithX:x andY:y andZ:z];
}

void triangular_face_geometric_vertices_callback(obj::index_type v1, obj::index_type v2, obj::index_type v3) {
}

void triangular_face_geometric_vertices_texture_vertices_callback(const obj::index_2_tuple_type& v1_vt1, const obj::index_2_tuple_type& v2_vt2, const obj::index_2_tuple_type& v3_vt3) {
}

void triangular_face_geometric_vertices_vertex_normals_callback(const obj::index_2_tuple_type& v1_vn1, const obj::index_2_tuple_type& v2_vn2, const obj::index_2_tuple_type& v3_vn3) {
	NSArray *vertices = [NSArray arrayWithObjects:
						 [NSNumber numberWithInt:std::tr1::get<0>(v1_vn1)],
						 [NSNumber numberWithInt:std::tr1::get<0>(v2_vn2)],
						 [NSNumber numberWithInt:std::tr1::get<0>(v3_vn3)],
						 nil];

	NSArray *normals = [NSArray arrayWithObjects:
						[NSNumber numberWithInt:std::tr1::get<1>(v1_vn1)],
						[NSNumber numberWithInt:std::tr1::get<1>(v2_vn2)],
						[NSNumber numberWithInt:std::tr1::get<1>(v3_vn3)],
						nil];

	[ref addTriangularFaceWithVerticesIndexes:vertices andNormalsIndexes:normals];
}

void triangular_face_geometric_vertices_texture_vertices_vertex_normals_callback(const obj::index_3_tuple_type& v1_vt1_vn1, const obj::index_3_tuple_type& v2_vt2_vn2, const obj::index_3_tuple_type& v3_vt3_vn3) {
}

void quadrilateral_face_geometric_vertices_callback(obj::index_type v1, obj::index_type v2, obj::index_type v3, obj::index_type v4) {
}

void quadrilateral_face_geometric_vertices_texture_vertices_callback(const obj::index_2_tuple_type& v1_vt1, const obj::index_2_tuple_type& v2_vt2, const obj::index_2_tuple_type& v3_vt3, const obj::index_2_tuple_type& v4_vt4) {
}

void quadrilateral_face_geometric_vertices_vertex_normals_callback(const obj::index_2_tuple_type& v1_vn1, const obj::index_2_tuple_type& v2_vn2, const obj::index_2_tuple_type& v3_vn3, const obj::index_2_tuple_type& v4_vn4) {
}

void quadrilateral_face_geometric_vertices_texture_vertices_vertex_normals_callback(const obj::index_3_tuple_type& v1_vt1_vn1, const obj::index_3_tuple_type& v2_vt2_vn2, const obj::index_3_tuple_type& v3_vt3_vn3, const obj::index_3_tuple_type& v4_vt4_vn4) {
}

void polygonal_face_geometric_vertices_begin_callback(obj::index_type v1, obj::index_type v2, obj::index_type v3) {
}

void polygonal_face_geometric_vertices_vertex_callback(obj::index_type v) {
}

void polygonal_face_geometric_vertices_end_callback() {
}

void polygonal_face_geometric_vertices_texture_vertices_begin_callback(const obj::index_2_tuple_type& v1_vt1, const obj::index_2_tuple_type& v2_vt2, const obj::index_2_tuple_type& v3_vt3) {
}

void polygonal_face_geometric_vertices_texture_vertices_vertex_callback(const obj::index_2_tuple_type& v_vt) {
}

void polygonal_face_geometric_vertices_texture_vertices_end_callback() {
}

void polygonal_face_geometric_vertices_vertex_normals_begin_callback(const obj::index_2_tuple_type& v1_vn1, const obj::index_2_tuple_type& v2_vn2, const obj::index_2_tuple_type& v3_vn3) {
}

void polygonal_face_geometric_vertices_vertex_normals_vertex_callback(const obj::index_2_tuple_type& v_vn) {
}

void polygonal_face_geometric_vertices_vertex_normals_end_callback() {
}

void polygonal_face_geometric_vertices_texture_vertices_vertex_normals_begin_callback(const obj::index_3_tuple_type& v1_vt1_vn1, const obj::index_3_tuple_type& v2_vt2_vn2, const obj::index_3_tuple_type& v3_vt3_vn3) {
}

void polygonal_face_geometric_vertices_texture_vertices_vertex_normals_vertex_callback(const obj::index_3_tuple_type& v_vt_vn) {
}

void polygonal_face_geometric_vertices_texture_vertices_vertex_normals_end_callback() {
}

@implementation WavefrontParser

- (void)dealloc {
	[self uncache];

	[super dealloc];
}

- (id)init {
	self = [super init];
	if (self) {
		triangles = [[NSMutableArray alloc] init];
	}
	return self;
}

- (void)parseIntoObject:(Object *)obj fromFile:(NSString *)path {
	[self parseIntoObject:obj fromFile:path withPrimitive:GL_TRIANGLES];
}

- (void)parseIntoObject:(Object *)obj fromFile:(NSString *)path withPrimitive:(GLenum)primitive {
	@synchronized([WavefrontParser class]) {
		[self uncache];

		if (ref) {
			[NSException raise:@"Invalid reference"
						format:@"The global reference isn't null"];
		}

		ref = self;

		engine.geometric_vertex_callback(geometric_vertex_callback);
		engine.vertex_normal_callback(vertex_normal_callback);
		engine.face_callbacks(triangular_face_geometric_vertices_callback,
							  triangular_face_geometric_vertices_texture_vertices_callback,
							  triangular_face_geometric_vertices_vertex_normals_callback,
							  triangular_face_geometric_vertices_texture_vertices_vertex_normals_callback,
							  quadrilateral_face_geometric_vertices_callback,
							  quadrilateral_face_geometric_vertices_texture_vertices_callback,
							  quadrilateral_face_geometric_vertices_vertex_normals_callback,
							  quadrilateral_face_geometric_vertices_texture_vertices_vertex_normals_callback,
							  polygonal_face_geometric_vertices_begin_callback,
							  polygonal_face_geometric_vertices_vertex_callback,
							  polygonal_face_geometric_vertices_end_callback,
							  polygonal_face_geometric_vertices_texture_vertices_begin_callback,
							  polygonal_face_geometric_vertices_texture_vertices_vertex_callback,
							  polygonal_face_geometric_vertices_texture_vertices_end_callback,
							  polygonal_face_geometric_vertices_vertex_normals_begin_callback,
							  polygonal_face_geometric_vertices_vertex_normals_vertex_callback,
							  polygonal_face_geometric_vertices_vertex_normals_end_callback,
							  polygonal_face_geometric_vertices_texture_vertices_vertex_normals_begin_callback,
							  polygonal_face_geometric_vertices_texture_vertices_vertex_normals_vertex_callback,
							  polygonal_face_geometric_vertices_texture_vertices_vertex_normals_end_callback);

        std::string filepath = [Utils getStrResourcePathForFile:path ofType:@"obj"];
		if (filepath == "") {
			[NSException raise:@"File not found"
						format:@"The .obj file path is invalid"];
		}

		NSTimeInterval timer = [NSDate timeIntervalSinceReferenceDate];

		engine.parse(filepath);

		timer = [NSDate timeIntervalSinceReferenceDate] - timer;

		NSLog(@"Parsing %@.obj...", path);

		int size = [triangles count];
		NSLog(@"Object parsed: %d faces (triangles), %d vertices", size, size * 3);

		NSTimeInterval lastTimer = [NSDate timeIntervalSinceReferenceDate];

		[self commitIntoObject:obj withPrimitive:primitive];

		timer += [NSDate timeIntervalSinceReferenceDate] - lastTimer;

		NSLog(@"Loading time: %.2f ms", timer * 1000.0);

		[self uncache];

		ref = nil;
	}
}

- (void)addVertexToCacheWithX:(float)x andY:(float)y andZ:(float)z {
	float *vertex = (float *)malloc(sizeof(M3DVector3f));
	vertex[0] = x;
	vertex[1] = y;
	vertex[2] = z;

	coords.push_back(vertex);
}

- (void)addNormalToCacheWithX:(float)x andY:(float)y andZ:(float)z {
	float *normal = (float *)malloc(sizeof(M3DVector3f));
	normal[0] = x;
	normal[1] = y;
	normal[2] = z;

	normals.push_back(normal);
}

- (void)addTextureCoordinateToCacheWithU:(float)u andV:(float)v {
	float *texCoord = (float *)malloc(sizeof(M3DVector2f));
	texCoord[0] = u;
	texCoord[1] = v;

	texCoords.push_back(texCoord);
}

- (void)addTriangularFaceWithVerticesIndexes:(NSArray *)vertices andNormalsIndexes:(NSArray *)normals_ {
	NSArray *face = [NSArray arrayWithObjects:vertices, normals_, nil];
	[triangles addObject:face];
}

- (void)commitIntoObject:(Object *)obj withPrimitive:(GLenum)primitive {
	int size = [triangles count];

	obj->Begin(primitive, size * 3);
	for (NSArray *t in triangles) {
		for (int i=0; i < 3; i++) {
			if ([t count] >= 2) {
				obj->Normal3f(normals[[(NSNumber *)[(NSArray *)[t objectAtIndex:1] objectAtIndex:i] intValue] - 1][0],
							  normals[[(NSNumber *)[(NSArray *)[t objectAtIndex:1] objectAtIndex:i] intValue] - 1][1],
							  normals[[(NSNumber *)[(NSArray *)[t objectAtIndex:1] objectAtIndex:i] intValue] - 1][2]);
			}
			obj->Color4f(1.0, 1.0, 1.0, 1.0);

			obj->Vertex3f(coords[[(NSNumber *)[(NSArray *)[t objectAtIndex:0] objectAtIndex:i] intValue] - 1][0],
						  coords[[(NSNumber *)[(NSArray *)[t objectAtIndex:0] objectAtIndex:i] intValue] - 1][1],
						  coords[[(NSNumber *)[(NSArray *)[t objectAtIndex:0] objectAtIndex:i] intValue] - 1][2]);
		}
	}
	obj->End();
}

- (void)uncache {
	[triangles removeAllObjects];

	for (int i=0; i < coords.size(); i++) {
		free(coords[i]);
	}
	coords.clear();

	for (int i=0; i < normals.size(); i++) {
		free(normals[i]);
	}
	normals.clear();

	for (int i=0; i < texCoords.size(); i++) {
		free(texCoords[i]);
	}
	texCoords.clear();
}

@end
