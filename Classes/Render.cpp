/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Render engine class.
 */

// Code adapted from "OpenGL SuperBible 5th Edition" (22th Sep. 2011)

#import "Render.h"

Render::Render(RuntimeInfo *info) {
	this->info = info;

	objManager = ObjectManager::getInstance();

	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glGenRenderbuffers(1, &renderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
}

bool Render::Initialize() {
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,
							  GL_COLOR_ATTACHMENT0,
							  GL_RENDERBUFFER,
							  renderbuffer);

	int framebufferWidth, framebufferHeight;
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &framebufferWidth);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);

	glGenRenderbuffers(1, &depthbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER,
						  GL_DEPTH_COMPONENT16,
						  framebufferWidth,
						  framebufferHeight);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER,
							  GL_DEPTH_ATTACHMENT,
							  GL_RENDERBUFFER,
							  depthbuffer);

	glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return false;
	}

	program = gltLoadShaderPairWithAttributes("iAR.vert",
											  "iAR.frag",
											  3,
											  GLT_ATTRIBUTE_VERTEX,
											  "Position",
											  GLT_ATTRIBUTE_NORMAL,
											  "Normal",
											  GLT_ATTRIBUTE_COLOR,
											  "SourceColor");
	if (!program) {
		return false;
	}
	glUseProgram(program);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	ChangeSize(info->viewportSz.width, info->viewportSz.height);
	CreateAxis();
	CreateBox();

	return true;
}

void Render::ChangeSize(int width, int height) {
	glViewport(0, 0, width, height);
	glScissor(0, 0, width, height);
}

void Render::Draw() {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLMatrixStack stack;
	stack.LoadIdentity();

	for (int i=0; i < queue.size(); i++) {
		Object *obj = objManager->objectWithID(queue[i].id);
		if (!obj) {
			continue;
		}

		M3DMatrix44f modelview;

		stack.PushMatrix();
		stack.MultMatrix(obj->modelview);
		stack.GetMatrix(modelview);
		stack.PopMatrix();

		M3DMatrix44f yaw;
		m3dRotationMatrix44(yaw, m3dDegToRad(obj->angle), 0.0, 1.0, 0.0);
		m3dMatrixMultiply44(modelview, modelview, yaw);

		M3DMatrix33f rotation;
		m3dExtractRotationMatrix33(rotation, modelview);

		M3DMatrix44f projection;
		stack.PushMatrix();
		stack.MultMatrix(info->projection);
		stack.GetMatrix(projection);
		stack.PopMatrix();

		GLuint projectionUniform = glGetUniformLocation(program, "Projection");
		glUniformMatrix4fv(projectionUniform, 1, 0, &projection[0]);

		GLuint modelviewUniform = glGetUniformLocation(program, "ModelView");
		glUniformMatrix4fv(modelviewUniform, 1, 0, &modelview[0]);

		GLuint rotationUniform = glGetUniformLocation(program, "Rotation");
		glUniformMatrix3fv(rotationUniform, 1, 0, &rotation[0]);

		if (info->flags.detailing) {
			axis.Draw();
			box.Draw();
		}
		else {
			obj->Draw();
		}
	}
	stack.PopMatrix();
}

void Render::CreateAxis() {
	axis.Begin(GL_LINES, 6);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(1.0, 0.0, 0.0, 1.0);
	axis.Vertex3f(0.0, 0.0, 0.0);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(0.0, 0.0, 0.0, 0.0);
	axis.Vertex3f(info->markerSz, 0.0, 0.0);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(0.0, 1.0, 0.0, 1.0);
	axis.Vertex3f(0.0, 0.0, 0.0);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(0.0, 0.0, 0.0, 0.0);
	axis.Vertex3f(0.0, info->markerSz, 0.0);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(0.0, 0.0, 1.0, 1.0);
	axis.Vertex3f(0.0, 0.0, 0.0);

	axis.Normal3f(0.0, 0.0, -1.0);
	axis.Color4f(0.0, 0.0, 0.0, 0.0);
	axis.Vertex3f(0.0, 0.0, info->markerSz);

	axis.End();
}

void Render::CreateBox() {
	box.Begin(GL_LINES, 24);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, 0.0, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, -info->markerSz / 2.0);

	box.Normal3f(0.0, 0.0, -1.0);
	box.Color4f(1.0, 0.0, 0.0, 1.0);
	box.Vertex3f(-info->markerSz / 2.0, info->markerSz, info->markerSz / 2.0);

	box.End();
}

void Render::Push(aruco::Marker &marker) {
	queue.push_back(marker);
}

void Render::Clear() {
	queue.clear();
}
