/**
 * @file
 * @author  Jonathan Hess (2011/2) <jhess666@gmail.com>
 * @author  Allan Camargo (2013/1)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * App view controller class.
 */

#import <UIKit/UIKit.h>

#import <aruco.hpp>
#import <cvdrawingutils.hpp>
#import <vector>

#import "CppUtils.h"
#import "glu.h"
#import "GLView.h"
#import "Object.h"
#import "ObjectManager.h"
#import "Types.h"
#import "VideoInput.h"
#import "WavefrontParser.h"

@interface iARViewController : UIViewController {
	RuntimeInfo info;

#if TARGET_OS_EMBEDDED
	VideoInput *videoInput;
#endif
	GLView *graphics;
	ObjectManager *objManager;

	aruco::CameraParameters cameraParams;
	aruco::MarkerDetector detector;
	std::vector<aruco::Marker> markers;

	NSTimeInterval detectionTime;
	CGPoint firstTouch;
}

@property (nonatomic, retain) UIView *interface;

/**
 * Configure all workspace data.
 */
- (void)setup;

/**
 * Call custom modifications routines.
 */
- (void)customize;

/**
 * Refresh all data inside custom views.
 */
- (void)refresh;

/**
 * Rotates the interface based on the angle
 *
 * @param angle to rotate
 */
- (void)rotateInterface:(int)angle;

/**
 * Show details clicked callback.
 *
 * @param sender The sender object
 */
- (IBAction)didDetailingButtonClick:(id)sender;

/**
 * Show log clicked callback.
 *
 * @param sender The sender object
 */
- (IBAction)didLoggingButtonClick:(id)sender;

/**
 * Show benchmark interface callback.
 *
 * @param sender Whoever sent this thing
 */
- (IBAction)didBenchMarkButtonClick:(id)sender;

/**
 * Device orientation changed callback.
 */
- (void)didDeviceOrientationChange:(NSNotification *)notification;

/**
 * Capture buffer processing finished callback.
 */
- (void)didCaptureBufferProcessingFinish:(NSNotification *)notification;

@end
