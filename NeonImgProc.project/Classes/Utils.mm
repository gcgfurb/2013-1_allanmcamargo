/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Objective-C++ utils.
 */

#import "Utils.h"

@implementation Utils

+ (std::string)getStrResourcePathForFile:(NSString *)filename ofType:(NSString *)ext {
	NSString *resourcePath = [[NSBundle mainBundle] pathForResource:filename
															 ofType:ext];
	if (!resourcePath) {
		NSLog(@"File not found: %s.%s", filename.UTF8String, ext.UTF8String);
		return "";
	}
	return [resourcePath cStringUsingEncoding:NSUTF8StringEncoding];
}

// Code adapted from "How to capture video frames from the camera as images
// using AV Foundation" (20th Sep. 2011)
// http://developer.apple.com/library/ios/#qa/qa1702/_index.html

+ (cv::Mat)createMatFromSampleBuffer:(CMSampleBufferRef)sampleBuffer withROI:(cv::Rect)roi {
	CVImageBufferRef imgBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(imgBuffer, 0);

	uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imgBuffer);

	size_t width = CVPixelBufferGetWidth(imgBuffer);
	size_t height = CVPixelBufferGetHeight(imgBuffer);

	cv::Mat img(cvSize(width, height), CV_8UC4, baseAddress);
	cv::Mat roiImg(img, roi);

	return roiImg;
}

+ (cv::Mat)cvMatWithImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to backing data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

+ (UIImage *)createUIImageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer {
	CVImageBufferRef imgBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(imgBuffer, 0);

	uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imgBuffer);

	size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imgBuffer);
	size_t width = CVPixelBufferGetWidth(imgBuffer);
	size_t height = CVPixelBufferGetHeight(imgBuffer);

	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

	CGContextRef context = CGBitmapContextCreate(baseAddress,
												 width,
												 height,
												 8,
												 bytesPerRow,
												 colorSpace,
												 kCGBitmapByteOrder32Little |
												 kCGImageAlphaPremultipliedFirst);

	CGImageRef quartzImg = CGBitmapContextCreateImage(context);
	CVPixelBufferUnlockBaseAddress(imgBuffer, 0);

	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);

	UIImage *uiImg = [UIImage imageWithCGImage:quartzImg];
	CGImageRelease(quartzImg);

	return uiImg;
}

+ (UIImage *)createUIImageFromMat:(cv::Mat)cvMat;
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize() * cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                     // Width
                                        cvMat.rows,                                     // Height
                                        8,                                              // Bits per component
                                        8 * cvMat.elemSize(),                           // Bits per pixel
                                        cvMat.step[0],                                  // Bytes per row
                                        colorSpace,                                     // Colorspace
                                        kCGImageAlphaNone | kCGBitmapByteOrderDefault,  // Bitmap info flags
                                        provider,                                       // CGDataProviderRef
                                        NULL,                                           // Decode
                                        false,                                          // Should interpolate
                                        kCGRenderingIntentDefault);                     // Intent
    
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return image;
}

+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
}

+ (void)saveUIImage:(UIImage *)img {
	UIImageWriteToSavedPhotosAlbum(img,
								   self,
								   @selector(image:didFinishSavingWithError:contextInfo:),
								   nil);
}

@end
