/**
 * @file    arucoARMUtilz.h
 * @author  Allan Camargo (2013/1)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Header for ARM specific code, including assembler.
 */

#ifndef iAR2_arucoARMUtilz_h
#define iAR2_arucoARMUtilz_h

#define iAR2_USE_ASM 1
#include <arm_neon.h>
#include <opencv/cv.h>

typedef enum
{
    BOX_FILTER_BLUR,
    MEAN_FILTER
} AsmFilterMethod;

/**
 *  @author Jonathan Hess (2011/2) <jhess666@gmail.com>
 *  @author	Allan Camargo (2013/1) (minor update, improved readability by using identifiers %[src] and %[dst]
 *
 *  Convert image to grayscale with NEON + ASM.
 *  Code adapted from "iOS Augmented Reality" (25th Sep. 2011)
 *  http://www.jonathansaggau.com/iOSAugmentedReality2011Boston.pdf
 *
 *  @param dst The destination data
 *  @param src The source data
 *  @param n The total number of pixels
 */
void asmNeonGrayScale(uint8_t * __restrict dst, const uint8_t * __restrict src, const int n)
{
	__asm__ __volatile__
	("lsr		%2, %2, #3			\n" // divide the total of pixels by 8 (we will process 8 pixels at a time), using shift
	 "# Build the three constants:	\n"
	 "mov		r4, #28				\n"
	 "mov		r5, #151			\n"
	 "mov		r6, #77				\n"
	 "vdup.8	d4, r4				\n" // store constants in NEON registers
	 "vdup.8	d5, r5				\n"
	 "vdup.8	d6, r6				\n"
	 "1:							\n" // main loop
	 "# Load 8 pixels:				\n"
	 "vld4.8	{d0-d3}, [%[src]]!	\n" // loads 3(RGB) NEON lanes with 8 values each...
	 "# Do the weight average:		\n" // ...from the pointer %[src] and then increments (!) it by the amount used by the instruction.
	 "vmull.u8	q7,	d0,	d4			\n" // multiplies
	 "vmlal.u8	q7,	d1,	d5			\n" // multiplies then accumulates
	 "vmlal.u8	q7,	d2,	d6			\n"
	 "# Shift and store:			\n"
	 "vshrn.u16	d7,	q7,	#8			\n" // since q7 is a quad-word register, we need to shift our values back to 8 bits
	 "vst1.8	{d7}, [%[dst]]!		\n" // save the resulting 8 pixels to our destination pointer %[dst]
	 "subs		%2, %2, #1			\n" // subtract 1 from our counter, and update our condition flags (sub[s])
	 "bne		1b					\n" // jump back to main loop if the flags say the last comparison wasn't equal to zero
	 : [dst] "+r" (dst)
	 : [src] "r" (src), "r"(n)
	 : "memory", "cc", "r4", "r5", "r6");
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Applies a mean filter to an image using NEON instructions
 *
 *	@param src 		Source data
 *	@param dst 		Destination pointer
 *	@param width	Width of the image
 *	@param height	Height of the image
 */
void asmNeonMeanFilter(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    const int indices[9] = { -(width + 1), -width, -(width - 1),
                                       -1,      0,    	      1,
                                width - 1,  width,    width + 1};
    
	__asm__ __volatile__
	("mul       r4, %[w], %[h]        \n" // r4 will be the first counter, starting at the size of the buffer
     "lsr       r4, r4, #3            \n" // dividing r4 by 8 since we'll process 8 pixels at a time
     "add       %[src], %[src], %[w]  \n" // skip the first line of pixels
     "add       %[dst], %[dst], %[w]  \n" // skip the first line of pixels
     "1:                              \n" // label 1
     "mov       r5, #9                \n" // r5 will be the second counter, starting at the size of the indices array
     "mov       r6, %[i]              \n" // r6 will point to (indices) in the loop
     "vmov.u8   d0, #0                \n" // d0 will be the accumulator
     "2:                              \n" // label 2
     "ldr       r7, [r6]              \n"
     "add       r8, %[src], r7        \n" // same as "const uint8_t* data = p + indices[i];"
     "vld1.8    {d1}, [r8]            \n" // d1 will contain the loaded data
     "vshr.u8   d2, d1, #3            \n" // shift right, 3 positions (in other words, divide by 8)
     "vsub.i8   d2, d1, d2            \n" // remove the difference, so the next shift will be closer to our target
     "vsra.u8   d0, d2, #3            \n" // shift right 3 again and then accumulate
     "add       r6, #4                \n" // increment r6
     "subs      r5, r5, #1            \n" // decrement r5
     "bne       2b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 2
     "add       %[src], #8            \n" // increment (src) pointer
     "vst1.8    {d0}, [%[dst]]!       \n" // store the result in (dst) and increment (dst) (!)
     "subs      r4, r4, #1            \n" // decrement r4
     "bne       1b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst)
	 : [src] "r" (src), [i] "r" (indices), [w] "r" (width), [h] "r" (height)
	 : "memory", "cc", "r4", "r5", "r6", "r7", "r8");
    /*
     
     Parameter block:
     : // output registers
     : // input registers
     : // clobber list
     
     
     - Output registers, these point to somewhere in the memory where we will write
     - Input registers, here we pass on to the compiler what we will use from the memory
     - Clobber list, here we tell the compiler which registers we'll use
     
     */
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Applies a static (blur kernel only) box filter to an image using NEON instructions
 *  Twice as fast as the last method (mean filter) and has better contour highlighting
 *
 *	@param src 		Source data
 *	@param dst 		Destination pointer
 *	@param width	Width of the image
 *	@param height	Height of the image
 */
void asmNeonBoxFilterBlurKernel(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    const int indices[4] = { -(width + 1), -(width - 1), width - 1, width + 1 };
    
	__asm__ __volatile__
	("mul           r4, %[w], %[h]        \n" // r4 will be the first counter, starting at the size of the buffer
     "lsr           r4, r4, #3            \n" // dividing r4 by 8 since we'll process 8 pixels at a time
     "add           %[src], %[src], %[w]  \n" // skip the first line of pixels
     "add           %[dst], %[dst], %[w]  \n" // skip the first line of pixels
     "1:                                  \n" // label 1
     "mov           r5, #4                \n" // r5 will be the second counter, starting at the size of the indices array
     "mov           r6, %[i]              \n" // r6 will point to (indices) in the loop
     "vmov.u8       d0, #0                \n" // d0 will be the accumulator
     "2:                                  \n" // label 2
     "ldr           r7, [r6]              \n"
     "add           r8, %[src], r7        \n" // same as "const uint8_t* data = p + indices[i];"
     "vld1.8        {d1}, [r8]            \n" // d1 will contain the loaded data
     "vsra.u8       d0, d1, #2            \n" // shift right 2 (divide by 4) and then accumulate
     "add           r6, #4                \n" // increment r6
     "subs          r5, r5, #1            \n" // decrement r5
     "bne           2b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 2
     "vst1.8        {d0}, [%[dst]]!       \n" // store the result in (dst)
     "add           %[src], #8            \n" // increment (src) pointer
     "subs          r4, r4, #1            \n" // decrement r4
     "bne           1b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst)
	 : [src] "r" (src), [i] "r" (indices), [w] "r" (width), [h] "r" (height)
	 : "memory", "cc", "r4", "r5", "r6", "r7", "r8");
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Execute threshold process on an image using NEON.
 *	Code adapted from the OpenCV library, method "cv::adaptiveThreshold"
 *
 *	@param _src 		Source data
 *	@param _dst 		Destination pointer
 *	@param maxValue		Maximum value to be assigned to a pixel
 *	@param delta
 */
void neonAdaptiveThreshold(const cv::InputArray _src, cv::OutputArray _dst, unsigned char maxValue, const int delta, const AsmFilterMethod filter)
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat mean;
    
    if( src.data != dst.data )
        mean = dst;
    
    if(filter)
        asmNeonMeanFilter(src.data, mean.data, size.width, size.height);
    else
        asmNeonBoxFilterBlurKernel(src.data, mean.data, size.width, size.height);
    
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    
    __asm__ __volatile__
  	("lsr       %[sz], %[sz], #3    \n" // dividing %[sz] by 8 since we'll process 8 pixels at a time
     "vdup.s8   d0, %[delta]        \n"
     "vdup.u8   d1, %[maxval]       \n"
     "vmov.u8   d2, #0              \n"
     "1:                            \n"
     "vld1.8    {d3}, [%[src]]!     \n"
     "vld1.8    {d4}, [%[m]]!       \n"
     "vsub.s8   d5, d3, d4          \n"
     "vcle.s8   d6, d5, d0          \n"
     "vbsl.u8   d6, d1, d2          \n"
     "vst1.8    {d6}, [%[dst]]!     \n"
     "subs      %[sz], %[sz], #1    \n" // decrement %[sz]
     "bne       1b                  \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst.data)
     : [src] "r" (src.data), [m] "r" (mean.data), [sz] "r" (size.width*size.height), [maxval] "r" (imaxval), [delta] "r" (-delta)
     : "memory", "cc");
}
#endif
